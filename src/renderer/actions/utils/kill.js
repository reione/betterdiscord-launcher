import path from "path";
import { getDiscordPath } from "../paths";
import { exec } from "child_process";
import findProcess from "find-process";
import kill from "tree-kill";
import { shell } from "electron";
import { progress } from "../../stores/installation";
import { log } from "./log";
import { remote } from "electron";

const platforms = {
    stable: "Discord",
    ptb: "Discord PTB",
    canary: "Discord Canary",
};
export default async function killProcesses(channels, progressPerLoop, shouldRestart = true) {
    for (const channel of channels) {
        let processName = platforms[channel];
        if (process.platform === "darwin") processName = platforms[channel]; // Discord Canary and Discord PTB on Mac
        else processName = platforms[channel].replace(" ", ""); // DiscordCanary and DiscordPTB on Windows/Linux

        const close = async () => {
            const results = await findProcess("name", processName, true);
            if (results || results.length) remote.app.exit();
            else setTimeout(() => close(), 1000);
        };

        log("Attempting to kill " + processName);
        try {
            const results = await findProcess("name", processName, true);
            if (!results || !results.length) {
                log(`✅ ${processName} not running, trying start process`);
                let discord = platforms[channel] == "canary" ? "Canary" : platforms[channel] == "ptb" ? "PTB" : "";
                const binPath = getDiscordPath(platforms[channel]).replace(/\\modules\\discord_desktop_core-.*\\discord_desktop_core/, `\\Discord${discord}.exe`);
                // const binPath = process.platform === "darwin" ? path.resolve("/Applications", `${processName}.app`) : processName;
                shell.openPath(binPath);
                progress.set(progress.value + progressPerLoop);
                close();
                setTimeout(() => remote.app.exit(), 10000);
                continue;
            }

            const parentPids = results.map((p) => p.ppid);
            const discordPid = results.find((p) => parentPids.includes(p.pid));
            const bin = process.platform === "darwin" ? path.resolve(discordPid.bin, "..", "..", "..") : discordPid.bin;
            await new Promise((r) => kill(discordPid.pid, r));
            if (shouldRestart) {
                setTimeout(() => {
                    shell.openPath(bin);
                    close();
                }, 1000);
            }
            progress.set(progress.value + progressPerLoop);
        } catch (err) {
            const symbol = shouldRestart ? "⚠️" : "❌";
            log(`${symbol} Could not kill ${platforms[channel]}`);
            log(`${symbol} ${err.message}`);
            return err;
        }
    }
}

